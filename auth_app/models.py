from django.contrib.auth.models import AbstractUser, Group
from django.db import models


class MyUser(AbstractUser):

    name = models.CharField(
        "Name",
        max_length=150,
        help_text='150 characters or fewer. Letters, digits and @/./+/-/_ only.',
        blank=False,
        null=False
    )
    username = models.EmailField(
        "Email",
        max_length=80,
        unique=True,
        error_messages={
            'unique': "A user with that email already exists.",
        },
        blank=True
    )

    group = models.ForeignKey(Group, on_delete=models.CASCADE, blank=True, null=True)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ["name", "password"]

    def __str__(self):
        return f"{self.username}"
