from django.contrib.auth.forms import UserCreationForm
from .models import MyUser


class MyCustomUserForm(UserCreationForm):

    class Meta:
        model = MyUser
        fields = ("name", "username")