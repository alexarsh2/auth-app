from django.shortcuts import render, redirect, reverse
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from auth_app.forms import MyCustomUserForm


# Views
@login_required
def home(request):
    return render(request, "registration/dashboard.html", {})


def register(request):
    if request.method == 'POST':
        form = MyCustomUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('home')
    else:
        form = MyCustomUserForm()
    return render(request, 'registration/register.html', {'form': form})
