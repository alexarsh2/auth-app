# Auth app

## Introduction

The goal of this project is to provide minimalistic django project for register and login

## Getting Started

The project uses python3 and this instructions assume you have it installed.

First clone the repository.
    
Install project dependencies:

    $ pip install -r requirements.txt
    
    
Then simply apply the migrations:

    $ python manage.py migrate
    

You can now run the development server:

    $ python manage.py runserver

Access it on http://localhost:8000
